const halson = require("halson");

let hal;
beforeAll(() => {
  hal = halson({ name: "John Doe" });
});
afterEach(() => {
  hal.removeLinks("self");
  hal.removeLinks("related");
});

describe("HATEOAS", () => {
  test("should return a HAL document with a self link", () => {
    hal.addLink("self", "/users/1");
    expect(hal).toEqual({
      _links: {
        self: {
          href: "/users/1",
        },
      },
      name: "John Doe",
    });
  });
  test("should return a HAL document with a link to a related resource", () => {
    hal.addLink("related", "/posts");
    expect(hal).toEqual({
      _links: {
        related: {
          href: "/posts",
        },
      },
      name: "John Doe",
    });
  });
  test("should get self link", () => {
    hal.addLink("self", "/users/1");
    const self = hal.getLink("self");
    expect(self).toEqual({
      href: "/users/1",
    });
  });
  test("should get the related link", () => {
    hal.addLink("related", "/posts");
    const related = hal.getLink("related");
    expect(related).toEqual({
      href: "/posts",
    });
  });
  test("should get all the related links", async () => {
    hal.addLink("related", [
      { id: 1, title: "First Post" },
      { id: 2, title: "Second Post" },
      { id: 3, title: "Third Post" },
    ]);
    const postsHal = await hal.getLinks("related");

    expect(postsHal).toEqual([
      { id: 1, title: "First Post" },
      { id: 2, title: "Second Post" },
      { id: 3, title: "Third Post" },
    ]);
  });
});