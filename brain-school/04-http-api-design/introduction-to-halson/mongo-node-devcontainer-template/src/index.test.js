import identity from "./index";
describe("index.js", () => {
  it("returns the first argument unchanged", () => {
    expect(identity(1)).toEqual(1);
  });
});
