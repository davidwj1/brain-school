const { v4: uuidv4 } = require("uuid");

class Task {
  constructor(name, description, id = uuidv4()) {
    this.id = id;
    this.name = name;
    this.description = description;
  }
}

module.exports = Task;
