const TaskRepository = require("./task-repository");

class InMemoryStorage extends TaskRepository {
  constructor() {
    super();
    this.tasks = new Map();
  }

  async create(task) {
    this.tasks.set(task.id, task);
  }
  async getAll() {
    const iterator = this.tasks.values();
    return { tasks: Array.from(iterator) };
  }

  async getById(id) {
    let task;
    if (id !== undefined) {
      task = this.tasks.get(id.toString());
    }
    if (task !== undefined) {
      return task;
    }
    return {};
  }

  async searchByName(taskName) {
    const taskArray = [];
    for (let [key, value] of this.tasks.entries()) {
      if (value.name === taskName) {
        taskArray.push(value);
      }
    }
    return taskArray;
  }

  async update(task) {
    const getTask = await this.getById(task.id);
    if (getTask !== null && JSON.stringify(getTask) !== "{}") {
      this.tasks.set(task.id, {
        name: task.name,
        description: task.description,
        id: task.id,
      });
      return "success";
    }
    return "fail";
  }
  async delete(id) {
    const deleteStatus = this.tasks.delete(id);
    return deleteStatus;
  }
}

module.exports = InMemoryStorage;
