const TaskRepository = require("./task-repository");
const mongoose = require("mongoose");

class MongoStorage extends TaskRepository {
  constructor() {
    super();
  }

  async setup() {
    await mongoose.connect("mongodb://localhost/tasks", {
      useNewUrlParser: true,
      useUnifiedTopology: true,
    });
    this.TaskSchema = new mongoose.Schema({
      _id: String,
      name: String,
      description: String,
    });
    this.Task = mongoose.model("Task", this.TaskSchema);
  }

  convertToMongo(task) {
    if (task) {
      return {
        _id: task.id,
        name: task.name,
        description: task.description,
      };
    }
    return null;
  }

  convertFromMongo(task) {
    if (task) {
      return {
        id: task._id,
        name: task.name,
        description: task.description,
      };
    }
    return null;
  }

  async create(newTask) {
    const task = new this.Task(this.convertToMongo(newTask));
    await task.save();
  }

  async getAll() {
    const tasks = await this.Task.find();
    return { tasks: tasks.map(this.convertFromMongo) };
  }

  async getById(id) {
    const mongoTask = await this.Task.findById(id).exec();
    return this.convertFromMongo(mongoTask);
  }

  async searchByName(taskName) {
    const mongoTaskList = await this.Task.find({ name: taskName }).exec();
    const searchArrayResult = [];
    mongoTaskList.forEach((task) => {
      searchArrayResult.push(this.convertFromMongo(task));
    });
    return searchArrayResult;
  }

  async update(task) {
    await this.Task.findByIdAndUpdate(task.id, task);
  }

  async delete(id) {
    const deleteStatus = await this.Task.findByIdAndRemove(id);
    if (deleteStatus !== null) {
      return true;
    }
    return false;
  }

  async cleanUp() {
    //await this.Task.deleteMany({});
    //mongoose.deleteModel(/.+/);
    //Delete above for production
    await mongoose.connection.close();
  }
}

module.exports = MongoStorage;
