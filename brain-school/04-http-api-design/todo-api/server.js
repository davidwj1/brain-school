const ServerApp = require("./src/server-api");
const MongoStorage = require("./src/repository/mongo-storage");
const process = require("process");

const server = new ServerApp(new MongoStorage());
server.start();

process.on("SIGINT", () => {
  server.stop();
});
