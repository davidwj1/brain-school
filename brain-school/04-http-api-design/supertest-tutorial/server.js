express = require("express");

class serverApp {
  constructor() {
    this.app = express();
    this.app.use(express.json());
    this.app.get("/", (req, res) => {
      res.status(200).send("Hello, World!");
    });

    this.app.post("/resource", (req, res) => {
      if ("name" in req.body) {
        res.status(201).send({ id: "1", name: `${req.body.name}` });
      } else {
        res.status(400).send({ error: "Wrong key value" });
      }
    });

    this.app.get("/search", (req, res) => {
      if (req.query.name === "test") {
        res.status(200).send([{ id: "1", name: "test" }]);
      } else {
        res.status(200).send([{}]);
      }
    });

    this.app.delete("/resource/:id", (req, res) => {
      res.status(204).send();
    });

    this.app.put("/resource/:id", (req, res) => {
      res.status(200).send({id: req.params.id, name: req.body.newName});
    });

  }

  startServer() {
    const serverPromise = new Promise((resolve, reject) => {
      this.server = this.app.listen(3000, () => {
        console.log("Server is running on port 3000");
        resolve();
      });
    });
    return serverPromise;
  }

  stopServer() {
    const serverPromise = new Promise((resolve, reject) => {
      this.server.close(() => {
        console.log("Server has been closed");
        resolve();
      });
    });
    return serverPromise;
  }
}

module.exports = serverApp;
