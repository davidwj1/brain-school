express = require("express");
const bodyParser = require("body-parser");
const halson = require("halson");
const Book = require('./repository/book')

class ServerApp {
  constructor(bookRepository) {
    this.bookRepository = bookRepository;

    this.app = express();
    this.app.use(bodyParser.json());

    this.app.get("/books", async (req, res) => {
      const books = await this.bookRepository.listAll();

      const resource = halson({
        title: "Bookshop API",
        items: books.map((book) => {
          const bookResource = halson(book);
          bookResource.addLink("self", `/books/${book.id}`);
          return bookResource;
        }),
      });

      resource.addLink("self", "/books");
      resource.addLink("create", { href: "/books", method: "POST" });
      res.json(resource);
    });

    this.app.get("/books/:id", async (req, res) => {
      const book = await this.bookRepository.getById(req.params.id);

      if (!book) {
        res.sendStatus(404);
        return;
      }

      const resource = halson(book)
        .addLink("self", `/books/${book.id}`)
        .addLink("collection", "/books");

      res.json(resource);
    });

    this.app.post("/books", async (req, res) => {
      const title = req.body.title;
      const author = req.body.author;
      const price = req.body.price;
      const book = new Book(title, author, price);
      await this.bookRepository.save(book);

      const resource = halson(book)
        .addLink("self", `/books/${book.id}`)
        .addLink("collection", "/books");

      res.status(201).json(resource);
    });
  }

  start() {
    const serverPromise = new Promise((resolve, reject) => {
      this.server = this.app.listen(3000, async () => {
        await this.bookRepository.setup();
        resolve();
      });
    });
    return serverPromise;
  }

  stop() {
    const serverPromise = new Promise((resolve, reject) => {
      this.server.close(async () => {
        await this.bookRepository.cleanUp();
        resolve();
      });
    });
    return serverPromise;
  }
}

module.exports = { ServerApp};
