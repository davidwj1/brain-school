const mongoose = require("mongoose");
(async () => {
  const mongoURI = `mongodb://${process.env.MONGO_USER}:${process.env.MONGO_PASSWORD}@mongo:27017/${process.env.MONGO_INITDB_DATABASE}`;
  await mongoose.connect(mongoURI, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  });
  const UserSchema = new mongoose.Schema({
    first: String,
    last: String,
  });

  const User = mongoose.model("User", UserSchema);
  const user = new User({ first: "David", last: "Wong" });
  user.save();
  const result = await User.find();
  console.log(result);
  await mongoose.connection.close();
})();
