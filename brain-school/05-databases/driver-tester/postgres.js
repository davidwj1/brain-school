const { Sequelize, DataTypes } = require("sequelize");

const sequelize = new Sequelize("brain-school-05", "david", "david", {
  host: "localhost",
  dialect: "postgres",
});

const User = sequelize.define("User", {
  firstName: {
    type: DataTypes.STRING,
  },
  lastName: {
    type: DataTypes.STRING,
  },
});

(async () => {
  console.log("Connecting to DB");
  await sequelize.sync({ force: true });

  console.log("Creating new User");
  const newUser = await User.create({
    firstName: "John",
    lastName: "Doe",
  });

  console.log("Finding users");
  const response = await User.findAll();
  console.log(response[0]);

  console.log("Updating user");
  await User.update({ lastName: "Changed" }, { where: { firstName: "John" } });

  console.log("Finding users after update");
  const responseAfterUpdate = await User.findAll();
  console.log(responseAfterUpdate);

  console.log("Deleting user");
  await User.destroy({
    where: {
      firstName: "John",
    },
  });

  console.log("Finding users after delete");
  const responseAfterDelete = await User.findAll();
  console.log(responseAfterDelete);

  console.log("Closing Connection to DB");
  await sequelize.close();
})();
