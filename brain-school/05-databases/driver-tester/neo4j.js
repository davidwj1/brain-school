const neo4j = require("neo4j-driver");

(async () => {
  const driver = neo4j.driver(
    "neo4j://localhost",
    neo4j.auth.basic("neo4j", "lovememahlots")
  );
  const session = driver.session();
  const first = "John";
  const last = "Doe";

  console.log("Create User");
  await session.run(
    "CREATE (a:User {firstName: $firstName, lastName: $lastName}) RETURN a",
    {
      firstName: first,
      lastName: last,
    }
  );

  console.log("Display Users");
  const allUsers = await session.run("MATCH (u:User) RETURN u")
  allUsers.records.forEach(data => {
    console.log(data._fields[0]);
  });

  console.log("Update User");
  await session.run("MATCH (u:User {firstName: 'John'}) SET u.lastName = 'Changed' RETURN u")

  console.log("Finding users after updated");
  const allUsersAfterUpdate = await session.run("MATCH (u:User) RETURN u")
  allUsersAfterUpdate.records.forEach(data => {
    console.log(data._fields[0]);
  });

  console.log("Delete User");
  await session.run("MATCH (u:User {firstName: 'John'}) DELETE u")

  console.log("Finding users after deletion");
  const allUsersAfterDelete = await session.run("MATCH (u:User) RETURN u")
  allUsersAfterDelete.records.forEach(data => {
    console.log(data._fields[0]);
  });

  await session.close();
  await driver.close();
})();
