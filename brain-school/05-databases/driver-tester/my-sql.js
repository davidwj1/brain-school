const { DataSource, EntitySchema } = require("typeorm");

//connect

class User {
  constructor(id, firstName, lastName) {
    this.id = id;
    this.firstName = firstName;
    this.lastName = lastName;
  }
}

(async () => {
  const datasource = new DataSource({
    type: "mysql",
    host: "localhost",
    username: "david",
    password: "david",
    database: "brain-school-05",
    entities: [
      new EntitySchema({
        target: User,
        name: "User",
        columns: {
          id: {
            primary: true,
            generated: true,
          },
          firstName: {
            type: "varchar",
          },
          lastName: {
            type: "varchar",
          },
        },
      }),
    ],
    synchronize: true,
  });
  await datasource.initialize();

  const userRepository = datasource.getRepository("User");

  const newUser = new User();
  newUser.firstName = "John";
  newUser.lastName = "Doe";
  console.log("Create new User");
  await userRepository.save(newUser);

  console.log("Getting all users from db");
  const response = await userRepository.find();
  console.log(response);

  console.log("Updating user");
  await userRepository.update({ firstName: "John" }, { lastName: "Changed" });

  console.log("Getting all users from db after update");
  const responseAfterUpdate = await userRepository.find();
  console.log(responseAfterUpdate);

  console.log("Deleting User in db");
  await userRepository.delete({ firstName: "John" });

  console.log("Getting all users from db after delete");
  const responseAfterDelete = await userRepository.find();
  console.log(responseAfterDelete);

  console.log("Close connection to db");
  await datasource.destroy();
})();
