const mongoose = require("mongoose");

mongoose.connect("mongodb://david:david@localhost:27017/brain-school-05", {
  useNewUrlParser: true,
  useUnifiedTopology: true,
});

const db = mongoose.connection;

db.once("open", async () => {
  console.log("Connected to DB");
  const UserSchema = new mongoose.Schema({
    first: String,
    last: String,
  });

  const User = mongoose.model("User", UserSchema);

  console.log("Saving New User");
  const newUser = new User({ first: "Bob", last: "Smith" });
  await newUser.save();

  console.log("Finding All Users after Adding");
  const resultAfter = await User.find();
  console.log(resultAfter);

  console.log("Updating Bob User");
  await User.findOneAndUpdate({ first: "Bob" }, { last: "Changed" });

  console.log("Finding All Users after Updating");
  const resultAfterUpdate = await User.find();
  console.log(resultAfterUpdate);

  console.log("Deleting User");
  await User.findOneAndDelete({ first: "Bob" });

  console.log("Finding users after delete");
  const resultAfterDeletion = await User.find();
  console.log(resultAfterDeletion);

  console.log("Closing Connection to DB");
  await mongoose.connection.close();
});
