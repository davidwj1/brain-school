const path = require("path");
const createFilePathResolver = (requestingFilePath) => (requestedFilePath) => {
  return path.join(path.dirname(requestingFilePath), requestedFilePath);
};
module.exports = createFilePathResolver;
