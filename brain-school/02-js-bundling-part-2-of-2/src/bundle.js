const createModuleDependencyGraph = require("./create-module-dependency-graph");
const createModuleMap = require("./create-module-map");
const createRuntime = require("./create-runtime");
const fs = require("fs");
const path = require("path");
const bundle = ({ entryFile, entryFilePath, outputFolder }) => {
  const moduleDependencyGraph = createModuleDependencyGraph(
    entryFilePath,
    entryFile
  );
  const moduleMap = createModuleMap(moduleDependencyGraph);
  const runtime = createRuntime(moduleMap, path.join(entryFilePath, entryFile));
  fs.writeFileSync(path.join(outputFolder, "bundle.js"), runtime, "utf-8");
};

module.exports = bundle;
