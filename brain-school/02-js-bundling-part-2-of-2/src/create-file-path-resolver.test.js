const filePathResolver = require("./create-file-path-resolver.js");
describe("create-file-path-resolver", () => {
  describe("given the absolute path of a file", () => {
    describe("and the relative path to a requested file", () => {
      it("returns the absolute path of the requested file", () => {
        const requestingFilePath = "/path/to/requester.js";
        const relativePathToRequestedFile = "../some-other-file.js";
        const absolutePathToRequestedFile = "/path/some-other-file.js";
        expect(
          filePathResolver(requestingFilePath)(relativePathToRequestedFile)
        ).toEqual(absolutePathToRequestedFile);
      });
    });
  });
});
