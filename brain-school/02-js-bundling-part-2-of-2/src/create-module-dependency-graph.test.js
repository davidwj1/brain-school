const createModuleDependencyGraph = require("./create-module-dependency-graph.js");
const path = require("path");
describe("create-module-dependency-graph", () => {
  describe("given a single module: A", () => {
    it("returns a graph with a single node", () => {
      const relativePathToModuleB =
        "./test-modules/folder-a/folder-b/module-b.js";
      const absolutePathToModuleB = path.join(
        __dirname,
        "./test-modules/folder-a/folder-b/module-b.js"
      );
      expect(
        createModuleDependencyGraph(__dirname, relativePathToModuleB)
      ).toEqual(
        expect.objectContaining({
          filePath: absolutePathToModuleB,
          dependencies: [],
        })
      );
    });
  });
  describe("given 2 modules: B<--depends-on--A", () => {
    it("returns a graph with two nodes", () => {
      const relativePathToModuleA = "./test-modules/folder-a/module-a.js";
      const absolutePathToModuleA = path.join(
        __dirname,
        "./test-modules/folder-a/module-a.js"
      );
      const absolutePathToModuleB = path.join(
        __dirname,
        "./test-modules/folder-a",
        "./folder-b/module-b.js"
      );
      expect(
        createModuleDependencyGraph(__dirname, relativePathToModuleA)
      ).toEqual(
        expect.objectContaining({
          filePath: absolutePathToModuleA,
          dependencies: expect.arrayContaining([
            expect.objectContaining({
              filePath: absolutePathToModuleB,
              dependencies: [],
            }),
          ]),
        })
      );
    });
  });

  describe("given 3 modules: C<--depends-on--B<--depends-on--A", () => {
    it("returns a graph with three nodes", () => {
      const relativePathToModuleC = "./test-modules/module-c.js";
      const absolutePathToModuleC = path.join(
        __dirname,
        "./test-modules/module-c.js"
      );
      const absolutePathToModuleA = path.join(
        __dirname,
        "./test-modules/",
        "./folder-a/module-a.js"
      );
      const absolutePathToModuleB = path.join(
        __dirname,
        "./test-modules/folder-a",
        "./folder-b/module-b.js"
      );
      expect(
        createModuleDependencyGraph(__dirname, relativePathToModuleC)
      ).toEqual(
        expect.objectContaining({
          filePath: absolutePathToModuleC,
          dependencies: expect.arrayContaining([
            expect.objectContaining({
              filePath: absolutePathToModuleA,
              dependencies: expect.arrayContaining([
                expect.objectContaining({
                  filePath: absolutePathToModuleB,
                  dependencies: [],
                }),
              ]),
            }),
          ]),
        })
      );
    });
  });

  describe("given 2 modules: D <--> E that depend on each other. Circular Dependencies", () => {
    it("returns a graph with 2 nodes", () => {
      const relativePathToModuleD = "./test-modules/module-d.js";
      const absolutePathToModuleD = path.join(
        __dirname,
        "./test-modules/module-d.js"
      );
      const absolutePathToModuleE = path.join(
        __dirname,
        "./test-modules/module-e.js"
      );

      expect(
        createModuleDependencyGraph(__dirname, relativePathToModuleD)
      ).toEqual(
        expect.objectContaining({
          filePath: absolutePathToModuleD,
          dependencies:[
            expect.objectContaining({
              filePath:absolutePathToModuleE,
              dependencies: []
            })
          ]
        })
      );
    });
  });
});
