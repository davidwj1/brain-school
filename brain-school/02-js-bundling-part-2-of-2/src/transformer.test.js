const transformer = require("./transformer.js");
const createTransformer = require("./transformer.js").createTransformer;
const prettier = require("prettier");
const assertCodeTransformedCorrectly = (
  code,
  transformedCode,
  customTransformer = transformer
) =>
  expect(prettier.format(customTransformer(code), { parser: "babel" })).toEqual(
    prettier.format(transformedCode, { parser: "babel" })
  );
describe("transformer.js", () => {
  describe("import statements", () => {
    describe("default imports", () => {
      it("transforms a default import with no references", () => {
        const code = `import a from './module-a.js';`;
        const transformedCode = `const _temp = require('./module-a.js');`;
        assertCodeTransformedCorrectly(code, transformedCode);
      });
      it("transforms a default import with references", () => {
        const code = `import a from './module-a.js'; console.log(a)`;
        const transformedCode = `const _temp = require('./module-a.js'); console.log(_temp["default"])`;
        assertCodeTransformedCorrectly(code, transformedCode);
      });
    });
    describe("non-default imports", () => {
      it("transforms a non-default import with no references", () => {
        const code = `import {a} from './module-a.js';`;
        const transformedCode = `const _temp = require('./module-a.js');`;
        assertCodeTransformedCorrectly(code, transformedCode);
      });
      it("transforms a non-default import with references", () => {
        const code = `import {a} from './module-a.js'; console.log(a)`;
        const transformedCode = `const _temp = require('./module-a.js'); console.log(_temp["a"])`;
        assertCodeTransformedCorrectly(code, transformedCode);
      });
      it("transforms multiple non-default imports with no references", () => {
        const code = `import {a,b} from './module-a.js';`;
        const transformedCode = `const _temp = require('./module-a.js');`;
        assertCodeTransformedCorrectly(code, transformedCode);
      });
      it("transforms multiple non-default imports with references", () => {
        const code = `import {a,b} from './module-a.js'; console.log(a,b);`;
        const transformedCode = `const _temp = require('./module-a.js'); console.log(_temp["a"],_temp["b"])`;
        assertCodeTransformedCorrectly(code, transformedCode);
      });
      it("transforms renamed non-default imports with no references", () => {
        const code = `import {a as z} from './module-a.js';`;
        const transformedCode = `const _temp = require('./module-a.js');`;
        assertCodeTransformedCorrectly(code, transformedCode);
      });
      it("transforms renamed non-default imports with references", () => {
        const code = `import {a as z} from './module-a.js'; console.log(z);`;
        const transformedCode = `const _temp = require('./module-a.js'); console.log(_temp["a"])`;
        assertCodeTransformedCorrectly(code, transformedCode);
      });
      it("transforms renamed multiple non-default imports with no references", () => {
        const code = `import {a as z, b as y} from './module-a.js';`;
        const transformedCode = `const _temp = require('./module-a.js');`;
        assertCodeTransformedCorrectly(code, transformedCode);
      });
      it("transforms renamed multiple non-default imports with references", () => {
        const code = `import {a as z, b as y} from './module-a.js'; console.log(z,y);`;
        const transformedCode = `const _temp = require('./module-a.js'); console.log(_temp["a"],_temp["b"])`;
        assertCodeTransformedCorrectly(code, transformedCode);
      });
    });
    describe("default and non-default imports", () => {
      it("transforms default and non-default imports with no references", () => {
        const code = `import a,{b,c} from './module-a.js';`;
        const transformedCode = `const _temp = require('./module-a.js');`;
        assertCodeTransformedCorrectly(code, transformedCode);
      });
      it("transforms default and non-default imports with references", () => {
        const code = `import a,{b,c} from './module-a.js'; console.log(a,b,c)`;
        const transformedCode = `const _temp = require('./module-a.js'); console.log(_temp["default"],_temp["b"],_temp["c"]);`;
        assertCodeTransformedCorrectly(code, transformedCode);
      });
      it("transforms default and renamed non-default import with no references", () => {
        const code = `import a,{b as x,c} from './module-a.js'`;
        const transformedCode = `const _temp = require('./module-a.js');`;
        assertCodeTransformedCorrectly(code, transformedCode);
      });
      it("transforms default and renamed non-default import with references", () => {
        const code = `import a,{b as x,c} from './module-a.js'; console.log(a,x,c)`;
        const transformedCode = `const _temp = require('./module-a.js'); console.log(_temp["default"],_temp["b"],_temp["c"])`;
        assertCodeTransformedCorrectly(code, transformedCode);
      });
      it("transforms default and renamed non-default import with no references", () => {
        const code = `import a,{b as x,c as y} from './module-a.js';`;
        const transformedCode = `const _temp = require('./module-a.js');`;
        assertCodeTransformedCorrectly(code, transformedCode);
      });
      it("transforms default and renamed non-default imports with references", () => {
        const code = `import a,{b as x,c as y} from './module-a.js'; console.log(a,x,y)`;
        const transformedCode = `const _temp = require('./module-a.js'); console.log(_temp["default"],_temp["b"],_temp["c"])`;
        assertCodeTransformedCorrectly(code, transformedCode);
      });
    });
    describe("Multiple Imports", () => {
      describe("of the same module", () => {
        it("resolves each import independently with no references", () => {
          const code = `import {a as z} from './module-a.js'; import {b as y} from './module-a.js'; console.log(z,y)`;
          const transformedCode = `const _temp = require('./module-a.js'); const _temp2 = require('./module-a.js'); console.log(_temp["a"],_temp2["b"])`;
          assertCodeTransformedCorrectly(code, transformedCode);
        });
        it("resolves each import independently with references", () => {
          const code = `import {a as z} from './module-a.js'; import {b as y} from './module-a.js'; console.log(z,y)`;
          const transformedCode = `const _temp = require('./module-a.js'); const _temp2 = require('./module-a.js'); console.log(_temp["a"],_temp2["b"])`;
          assertCodeTransformedCorrectly(code, transformedCode);
        });
      })
      describe("of different modules", () => {
        it("resolves each import independently with no references", () => {
          const code = `import {a as z} from './module-a.js'; import {b as y} from './module-a.js';`;
          const transformedCode = `const _temp = require('./module-a.js'); const _temp2 = require('./module-a.js'); `;
          assertCodeTransformedCorrectly(code, transformedCode);
        });
        it("resolves each import independently with references", () => {
          const code = `import {a as z} from './module-a.js'; import {b as y} from './module-b.js'; console.log(z,y)`;
          const transformedCode = `const _temp = require('./module-a.js'); const _temp2 = require('./module-b.js'); console.log(_temp["a"],_temp2["b"])`;
          assertCodeTransformedCorrectly(code, transformedCode);
        });
      })
    })
    describe("path resolution", () => {
      it("rewrites module paths using a custom path resolver", () => {
        const code =
          //
          `
          import a from './module-a.js';
          import c,{d} from './module-b.js';`;
        const transformedCode =
          //
          `
          const _temp= require('/absolute/path/to/module-a.js');
          const _temp2 = require('/absolute-path-to/module-b.js')`;
        const pathResolutionMap = {
          "./module-a.js": "/absolute/path/to/module-a.js",
          "./module-b.js": "/absolute-path-to/module-b.js",
        };
        const customTransformer = createTransformer(
          (path) => pathResolutionMap[path]
        );
        assertCodeTransformedCorrectly(
          code,
          transformedCode,
          customTransformer
        );
      });
    });
  });
  describe("export statements", () => {
    describe("default export", () => {
      it("transforms a default export", () => {
        const code = `export default a;`;
        const transformedCode = `exports.default = a;`;
        assertCodeTransformedCorrectly(code, transformedCode);
      });
    });
    describe("non-default exports", () => {
      it("transforms a non-default class declaration", () => {
        const code = `export class FooBaz{}`;
        const transformedCode = `exports.FooBaz = class FooBaz{};`;
        assertCodeTransformedCorrectly(code, transformedCode);
      });
      it("transforms a non-default function declaration", () => {
        const code = `export function flimFlam(){};`;
        const transformedCode = `exports.flimFlam = function flimFlam(){};`;
        assertCodeTransformedCorrectly(code, transformedCode);
      });
      it("transforms non-default variable declarations", () => {
        const code = `export const zoomZoom = 3, flurpDerp = "simsim";`;
        const transformedCode = `const zoomZoom = 3; const flurpDerp = "simsim"; exports.zoomZoom = zoomZoom;  exports.flurpDerp = flurpDerp;`;
        assertCodeTransformedCorrectly(code, transformedCode);
      });
      it("transforms non-default variable declaration", () => {
        const code = `export const a = 3;`;
        const transformedCode = `const a = 3; exports.a = a;`;
        assertCodeTransformedCorrectly(code, transformedCode);
      });
      
    });
  });
});
