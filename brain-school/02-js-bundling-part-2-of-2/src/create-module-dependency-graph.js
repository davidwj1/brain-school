const path = require("path");
const babel = require("@babel/core");
const fs = require("fs");
const createTransformer = require("./transformer.js").createTransformer;
const createFilePathResolver = require("./create-file-path-resolver");

const getHasModuleBeenVisited = (visitedModules, absoluteModuleFilePath) => {
  return visitedModules.includes(absoluteModuleFilePath);
};

const createModuleDependencyGraph = (
  absolutePathOfParentModule,
  relativeFilePath,
  visitedModules = []
) => {
  const absoluteFilePath = path.join(
    absolutePathOfParentModule,
    relativeFilePath
  );
  if (!getHasModuleBeenVisited(visitedModules, absoluteFilePath)) {
    visitedModules.push(absoluteFilePath);
  }
  const _content = fs.readFileSync(absoluteFilePath, "utf-8");
  const ast = babel.parseSync(_content);
  const dependencies = ast.program.body
    .filter((node) => node.type === "ImportDeclaration")
    .map((node) => node.source.value)
    .filter((relativeFilePathOfDependency) => {
      let absolutePathOfDependency = path.join(
        path.dirname(absoluteFilePath),
        relativeFilePathOfDependency
      );
      return !getHasModuleBeenVisited(visitedModules, absolutePathOfDependency);
    })
    .map((relativeFilePathOfDependency) =>
      createModuleDependencyGraph(
        path.dirname(absoluteFilePath),
        relativeFilePathOfDependency,
        visitedModules
      )
    );

  return {
    filePath: absoluteFilePath,
    dependencies,
    get content() {
      delete this.content;
      const transformer = createTransformer(
        createFilePathResolver(this.filePath)
      );
      this.content = transformer(_content, this.filePath);
      return this.content;
    },
  };
};
module.exports = createModuleDependencyGraph;
