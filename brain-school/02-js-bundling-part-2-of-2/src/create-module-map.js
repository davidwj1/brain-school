const collectModules = (module, modules = []) => {
  modules.push(module);
  module.dependencies
    .filter((dependency) => {
      return (
        modules.findIndex(
          (collectedModule) => collectedModule.filePath === dependency.filePath
        ) === -1
      );
    })
    .forEach((dependency) => {
      collectModules(dependency, modules);
    });
  return modules;
};

const createModuleMap = (moduleDepedencyGraph) => {
  const modules = collectModules(moduleDepedencyGraph);
  const moduleEntries = modules
    .map(
      (module) =>
        `'${module.filePath}': function (exports,require){ ${module.content} }`
    )
    .join(",");
  return `{ ${moduleEntries} }`;
};
module.exports = createModuleMap;
