const babel = require("@babel/core");
const createTransformer = (pathResolver) => (code) => {
  const { code: transformedCode } = babel.transformSync(code, {
    plugins: [
      function transformerPlugin({ types }) {
        return {
          visitor: {
            ImportDeclaration(path) {
              const importedObjectName = path.scope.generateUidIdentifier();
      
              path.get("specifiers").forEach((specifier) => {
                const isImportDefaultSpecifier = specifier.isImportDefaultSpecifier();
                var importedKey = "default";
      
                if (!isImportDefaultSpecifier) {
                  const importedName = specifier.node.imported.name;
                  importedKey = isImportDefaultSpecifier ? "default" : importedName;
                }
      
                const localImportedName = specifier.node.local.name;
      
                const binding = specifier.scope.getBinding(localImportedName);
      
                binding.referencePaths.forEach((referencePath) => {
                  const replacementReference = types.memberExpression(
                    importedObjectName,
                    types.stringLiteral(importedKey),
                    true
                  );
                  referencePath.replaceWith(replacementReference);
                });
              });
      
              const modulePath = pathResolver(path.get("source.value").node);
              const requireCall = types.callExpression(types.identifier("require"), [
                types.stringLiteral(modulePath)
              ]);
              const requireAssignment = types.variableDeclaration("const", [
                types.variableDeclarator(importedObjectName, requireCall)
              ]);
              path.replaceWith(requireAssignment);
            },
            ExportDefaultDeclaration(path) {
              path.replaceWith(
                types.expressionStatement(
                  types.assignmentExpression(
                    "=",
                    types.memberExpression(
                      types.identifier("exports"),
                      types.identifier("default")
                    ),
                    types.toExpression(path.get("declaration").node)
                  )
                )
              );
            },
            ExportNamedDeclaration(path) {
              const declaration = path.get("declaration");

              if (
                declaration.isClassDeclaration() ||
                declaration.isFunctionDeclaration()
              ) {
                path.replaceWith(
                  types.expressionStatement(
                    types.assignmentExpression(
                      "=",
                      types.memberExpression(
                        types.identifier("exports"),
                        declaration.get("id").node
                      ),
                      types.toExpression(declaration.node)
                    )
                  )
                );
              } else if (declaration.isVariableDeclaration()) {
                const nodesVariableDeclarations = [];
                const nodesAssignmentExpressions = [];
                const variableDeclaration = path.node.declaration;
                const variableDeclarators = variableDeclaration.declarations;
                variableDeclarators.forEach((variableDeclarator) => {
                  const variableIdentifierName = variableDeclarator.id.name;
                  const variableInitialValue = variableDeclarator.init
                    ? variableDeclarator.init.value
                    : null;
                  const newVariableDeclaration = types.variableDeclaration(
                    variableDeclaration.kind,
                    [variableDeclarator]
                  );
                  const newMemberExpression = types.memberExpression(
                    types.identifier("exports"),
                    types.identifier(variableIdentifierName)
                  );
                  const newAssignmentExpression = types.assignmentExpression(
                    "=",
                    newMemberExpression,
                    types.identifier(variableIdentifierName)
                  );
                  nodesVariableDeclarations.push(newVariableDeclaration);
                  nodesAssignmentExpressions.push(newAssignmentExpression)
                });
                path.replaceWithMultiple(nodesVariableDeclarations.concat(nodesAssignmentExpressions));
              }
            },
          },
        };
      },
    ],
  });
  return transformedCode;
};

module.exports = createTransformer((path) => path);

module.exports.createTransformer = createTransformer;
