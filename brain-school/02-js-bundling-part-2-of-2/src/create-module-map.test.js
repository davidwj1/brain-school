const path = require("path");
const createModuleDependencyGraph = require("./create-module-dependency-graph.js");
const createModuleMap = require("./create-module-map.js");
describe("create-module-map", () => {
  describe("given a module dependency graph with a single module", () => {
    it("returns a module map with single entry", () => {
      const moduleDependencyGraph = createModuleDependencyGraph(
        __dirname,
        "./test-modules/folder-a/folder-b/module-b.js"
      );

      expect(createModuleMap(moduleDependencyGraph)).toMatchInlineSnapshot(`
        "{ '/home/david/brainSchool/graduate-programme-resources/brain-school/02-js-bundling-part-2-of-2/src/test-modules/folder-a/folder-b/module-b.js': function (exports,require){ const b = "bebe";
        exports.default = b; } }"
      `);
    });
  });
  describe("given a module dependency graph with a multiple, dependent modules", () => {
    it("returns a module map with an entry for each module", () => {
      const moduleDependencyGraph = createModuleDependencyGraph(
        __dirname,
        "./test-modules/module-c.js"
      );

      expect(createModuleMap(moduleDependencyGraph)).toMatchInlineSnapshot(`
        "{ '/home/david/brainSchool/graduate-programme-resources/brain-school/02-js-bundling-part-2-of-2/src/test-modules/module-c.js': function (exports,require){ const _temp = require("/home/david/brainSchool/graduate-programme-resources/brain-school/02-js-bundling-part-2-of-2/src/test-modules/folder-a/module-a.js");
        const z = "luluLimen";
        exports.z = z
        console.log(\`So yeah, \${_temp["default"]} \${z}\`); },'/home/david/brainSchool/graduate-programme-resources/brain-school/02-js-bundling-part-2-of-2/src/test-modules/folder-a/module-a.js': function (exports,require){ const _temp = require("/home/david/brainSchool/graduate-programme-resources/brain-school/02-js-bundling-part-2-of-2/src/test-modules/folder-a/folder-b/module-b.js");
        const a = "hey " + _temp["default"];
        exports.default = a; },'/home/david/brainSchool/graduate-programme-resources/brain-school/02-js-bundling-part-2-of-2/src/test-modules/folder-a/folder-b/module-b.js': function (exports,require){ const b = "bebe";
        exports.default = b; } }"
      `);
    });
  });
  describe("given a module dependency graph with cyclical dependency", () => {
    it("returns a module map with an entry for each module", () => {
      const moduleDependencyGraph = createModuleDependencyGraph(
        __dirname,
        "./test-modules/module-d.js"
      );

      expect(createModuleMap(moduleDependencyGraph)).toMatchInlineSnapshot(`
        "{ '/home/david/brainSchool/graduate-programme-resources/brain-school/02-js-bundling-part-2-of-2/src/test-modules/module-d.js': function (exports,require){ const _temp = require("/home/david/brainSchool/graduate-programme-resources/brain-school/02-js-bundling-part-2-of-2/src/test-modules/module-e.js");
        const d = "Module D";
        exports.d = d
        console.log(\`\${d} \${_temp["default"]}\`); },'/home/david/brainSchool/graduate-programme-resources/brain-school/02-js-bundling-part-2-of-2/src/test-modules/module-e.js': function (exports,require){ const _temp = require("/home/david/brainSchool/graduate-programme-resources/brain-school/02-js-bundling-part-2-of-2/src/test-modules/module-d.js");
        const e = "Module E";
        exports.e = e
        console.log(\`\${e} \${_temp["default"]}\`); } }"
      `);
    });
  });
});
