const express = require("express");
const bodyParser = require("body-parser");
const halson = require("halson");
const Task = require("./repository/task");

class ServerApp {
  constructor(taskRepository) {
    this.taskRepository = taskRepository;
    this.app = express();
    this.app.use(bodyParser.json());

    this.app.get("/api/task", async (req, res) => {
      let tasks
      if ("name" in req.query) {
        tasks = await this.taskRepository.searchByName(req.query.name);
      } else {
        tasks = await this.taskRepository.getAll();
      }
      res.status(200).json(tasks);
    });

    this.app.get("/api/task/:id", async (req, res) => {
      const task = await this.taskRepository.getById(req.params.id);
      res.json(task);
    });

    this.app.post("/api/task", async (req, res) => {
      const taskName = req.body.name;
      const taskDescription = req.body.description;
      const newTask = new Task(taskName, taskDescription);
      await this.taskRepository.create(newTask);
      res.status(200).send("Created new task");
    });

    this.app.put("/api/task/:id", async (req, res) => {
      await this.taskRepository.update(req.body);
      res.status(200).send("Successful Update");
    });

    this.app.delete("/api/task/:id", async (req, res) => {
      await this.taskRepository.delete(req.params.id);
      res.status(200).send();
    });
  }

  start() {
    const serverPromise = new Promise((resolve, reject) => {
      this.server = this.app.listen(3000, async () => {
        resolve();
      });
    });
    return serverPromise;
  }

  stop() {
    const serverPromise = new Promise((resolve, reject) => {
      this.server.close(async () => {
        resolve();
      });
    });
    return serverPromise;
  }
}

module.exports = ServerApp;
