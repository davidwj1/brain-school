const Task = require("../repository/task.js");
const InMemoryTaskRepository = require("../repository/in-memory-storage.js");
const request = require("supertest");
const ServerApp = require("../task-api.js");

describe("Task API", () => {
  let server;

  beforeEach(async () => {
    const taskRepository = new InMemoryTaskRepository();
    server = new ServerApp(taskRepository);
    await server.start();
  });

  afterEach(async () => {
    await server.stop();
  });

  //Helper test function to create tasks
  const createTasks = async (numTimesToRepeat) => {
    for (let ii = 1; ii <= numTimesToRepeat; ii++) {
      await request(server.app)
        .post("/api/task")
        .send(new Task(`Test${ii}`, `Desc${ii}`));
    }
  };

  describe("GET Tasks", () => {
    it("Responds with status code 200", async () => {
      const response = await request(server.app).get("/api/task");
      expect(response.statusCode).toBe(200);
    });
    it("Responds with JSON", async () => {
      const responseData = await request(server.app).get("/api/task");
      expect(responseData.type).toBe("application/json");
    });
    it("Responds with empty list", async () => {
      const responseData = await request(server.app).get("/api/task");
      expect(responseData.body).toEqual({ tasks: [] });
    });
    describe("Get Task with ID", () => {
      it("Responds with 1 task out of list of 1", async () => {
        await createTasks(1);
        const responseData = await request(server.app).get("/api/task");
        const taskId = responseData.body.tasks[0].id;
        const specifiedResourceData = await request(server.app).get(
          `/api/task/${taskId}`
        );
        expect(specifiedResourceData.body).toEqual({
          name: "Test1",
          description: "Desc1",
          id: expect.any(String),
        });
      });
      it("Responds with 2nd task out of list of 3", async () => {
        await createTasks(3);
        const responseData = await request(server.app).get("/api/task");
        const taskId = responseData.body.tasks[1].id;
        const specifiedResourceData = await request(server.app).get(
          `/api/task/${taskId}`
        );
        expect(specifiedResourceData.body).toEqual({
          name: "Test2",
          description: "Desc2",
          id: taskId,
        });
      });
    });
    describe("Search task with name", () => {
      it("Responds with 1 matching task out of list of 3", async () => {
        await createTasks(3);
        const response = await request(server.app)
          .get("/api/task")
          .query({ name: "Test2" });
        expect(response.body).toEqual([
          { name: "Test2", description: "Desc2", id: expect.any(String) },
        ]);
      });
      it("Responds with 2 matching tasks out of list of 3", async () => {
        await request(server.app)
          .post("/api/task")
          .send(new Task("Test1", "Desc1"));
        await request(server.app)
          .post("/api/task")
          .send(new Task("Test1", "Desc123"));
        await request(server.app)
          .post("/api/task")
          .send(new Task("Test2", "Desc2"));
        const response = await request(server.app)
          .get("/api/task")
          .query({ name: "Test1" });
        expect(response.body).toEqual([
          { name: "Test1", description: "Desc1", id: expect.any(String) },
          { name: "Test1", description: "Desc123", id: expect.any(String) },
        ]);
      });
    });
  });
  describe("POST Tasks", () => {
    it("Creates 1 new task", async () => {
      await createTasks(1);
      const responseData = await request(server.app).get("/api/task");
      expect(responseData.body.tasks[0].name).toBe("Test1");
      expect(responseData.body.tasks[0].description).toBe("Desc1");
    });
    it("Creates 2 new tasks", async () => {
      await createTasks(2);
      const responseData = await request(server.app).get("/api/task");
      expect(responseData.body.tasks[0].name).toBe("Test1");
      expect(responseData.body.tasks[0].description).toBe("Desc1");
      expect(responseData.body.tasks[1].name).toBe("Test2");
      expect(responseData.body.tasks[1].description).toBe("Desc2");
    });
  });

  describe("PUT Tasks", () => {
    it("Update task in list of 1", async () => {
      await createTasks(1);
      const responseData = await request(server.app).get("/api/task");
      const specificTaskId = responseData.body.tasks[0].id;
      const specifiedTask = (
        await request(server.app).get(`/api/task/${specificTaskId}`)
      ).body;

      specifiedTask.name = "changedName";
      specifiedTask.description = "changedDescription";

      await request(server.app)
        .put(`/api/task/${specificTaskId}`)
        .send(specifiedTask);
      const response = (
        await request(server.app).get(`/api/task/${specificTaskId}`)
      ).body;
      expect(response).toEqual({
        name: "changedName",
        description: "changedDescription",
        id: expect.any(String),
      });
    });
    it("Update 2nd task in list of 3", async () => {
      await createTasks(3);
      const responseData = await request(server.app).get("/api/task");
      const specificTaskId = responseData.body.tasks[1].id;
      const specifiedTask = (
        await request(server.app).get(`/api/task/${specificTaskId}`)
      ).body;
      specifiedTask.name = "changedName";
      specifiedTask.description = "changedDescription";
      await request(server.app)
        .put(`/api/task/${specificTaskId}`)
        .send(specifiedTask);
      const response = (await request(server.app).get(`/api/task`)).body;
      expect(response).toEqual({
        tasks: [
          { name: "Test1", description: "Desc1", id: expect.any(String) },
          {
            name: "changedName",
            description: "changedDescription",
            id: expect.any(String),
          },
          { name: "Test3", description: "Desc3", id: expect.any(String) },
        ],
      });
    });
  });
  describe("DELETE Tasks", () => {
    it("Deletes task in list of 1", async () => {
      await createTasks(1);
      const responseData = await request(server.app).get("/api/task");
      const taskId = responseData.body.tasks[0].id;
      await request(server.app).delete(`/api/task/${taskId}`);
      const responseDataAfterDeletion = await request(server.app).get(
        "/api/task"
      );
      expect(responseDataAfterDeletion.body).toEqual({ tasks: [] });
    });
    it("Deletes 2nd task in list of 3", async () => {
      await createTasks(3);
      const responseData = await request(server.app).get("/api/task");
      const taskId = responseData.body.tasks[1].id;
      await request(server.app).delete(`/api/task/${taskId}`);
      const responseDataAfterDeletion = await request(server.app).get(
        "/api/task"
      );
      expect(responseDataAfterDeletion.body.tasks[0].name).toBe("Test1");
      expect(responseDataAfterDeletion.body.tasks[0].description).toBe("Desc1");
      expect(responseDataAfterDeletion.body.tasks[1].name).toBe("Test3");
      expect(responseDataAfterDeletion.body.tasks[1].description).toBe("Desc3");
    });
  });
});
