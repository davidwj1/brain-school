const InMemoryTaskRepository = require("../repository/in-memory-storage.js");
const Task = require("../repository/task.js");

describe("In memory repository Tests", () => {
  let taskRepository;
  beforeEach(() => {
    taskRepository = new InMemoryTaskRepository();
  });

  //Helper to create tasks
  const createTasks = async (numTimesToCreate) => {
    for (let ii = 1; ii <= numTimesToCreate; ii++) {
      await taskRepository.create(new Task(`Test${ii}`, `Desc${ii}`));
    }
  };

  //Helper to get specific id
  const getSpecificTaskId = async (index) => {
    const data = await taskRepository.getAll();
    return data.tasks[index].id;
  };

  describe("Creating Tasks", () => {
    it("Create 1 task", async () => {
      await createTasks(1);
      const data = await taskRepository.getAll();
      expect(data).toEqual({
        tasks: [
          { name: "Test1", description: "Desc1", id: expect.any(String) },
        ],
      });
    });
    it("Create 3 tasks", async () => {
      await createTasks(3);
      const data = await taskRepository.getAll();
      expect(data).toEqual({
        tasks: [
          { name: "Test1", description: "Desc1", id: expect.any(String) },
          { name: "Test2", description: "Desc2", id: expect.any(String) },
          { name: "Test3", description: "Desc3", id: expect.any(String) },
        ],
      });
    });
  });
  describe("Getting Tasks", () => {
    it("Get all tasks", async () => {
      await createTasks(3);
      const data = await taskRepository.getAll();
      expect(data).toEqual({
        tasks: [
          { name: "Test1", description: "Desc1", id: expect.any(String) },
          { name: "Test2", description: "Desc2", id: expect.any(String) },
          { name: "Test3", description: "Desc3", id: expect.any(String) },
        ],
      });
    });
    it("Get 2nd task out of 3", async () => {
      await createTasks(3);
      const specificTaskId = await getSpecificTaskId(1);
      const specifiedData = await taskRepository.getById(specificTaskId);
      expect(specifiedData).toEqual({
        name: "Test2",
        description: "Desc2",
        id: expect.any(String),
      });
    });
    it("Getting non-existent task", async () => {
      const response = await taskRepository.getById("1234");
      expect(response).toEqual({});
    });
    it("Search matching 2 task by name", async () => {
      await createTasks(3);
      const response = await taskRepository.searchByName("Test1");
      expect(response).toEqual([
        { name: "Test1", description: "Desc1", id: expect.any(String) },
      ]);
    });
    it("Search matching 2 tasks by name", async () => {
      await createTasks(3);
      await taskRepository.create(new Task("Test1", "Desc123"));
      const response = await taskRepository.searchByName("Test1");
      expect(response).toEqual([
        { name: "Test1", description: "Desc1", id: expect.any(String) },
        { name: "Test1", description: "Desc123", id: expect.any(String) },
      ]);
    });
  });
  describe("Updating Tasks", () => {
    it("Update a task name", async () => {
      await createTasks(1);
      const specificTaskId = await getSpecificTaskId(0);
      const specifiedTask = await taskRepository.getById(specificTaskId);
      specifiedTask.name = "TestChange";
      await taskRepository.update(specifiedTask);
      const response = await taskRepository.getById(specificTaskId);
      expect(response).toEqual({
        name: "TestChange",
        description: expect.any(String),
        id: expect.any(String),
      });
    });
    it("Update a task description", async () => {
      await createTasks(1);
      const specificTaskId = await getSpecificTaskId(0);
      const specifiedTask = await taskRepository.getById(specificTaskId);
      specifiedTask.description = "changedDescription";
      await taskRepository.update(specifiedTask);
      const response = await taskRepository.getById(specificTaskId);
      expect(response).toEqual({
        name: expect.any(String),
        description: "changedDescription",
        id: expect.any(String),
      });
    });
    it("Update a task name and description", async () => {
      await createTasks(1);
      const specificTaskId = await getSpecificTaskId(0);
      const specifiedTask = await taskRepository.getById(specificTaskId);
      specifiedTask.name = "changedName";
      specifiedTask.description = "changedDescription";
      await taskRepository.update(specifiedTask);
      const response = await taskRepository.getById(specificTaskId);
      expect(response).toEqual({
        name: "changedName",
        description: "changedDescription",
        id: expect.any(String),
      });
    });
  });
  describe("Deleting Tasks", () => {
    it("Delete 1 task out of list of 1", async () => {
      await createTasks(1);
      const specificTaskId = await getSpecificTaskId(0);
      await taskRepository.delete(specificTaskId);
      const response = await taskRepository.getAll();
      expect(response).toEqual({ tasks: [] });
    });
    it("Delete 1st task out of list of 3", async () => {
      await createTasks(3);
      const specificTaskId = await getSpecificTaskId(0);
      await taskRepository.delete(specificTaskId);
      const response = await taskRepository.getAll();
      expect(response).toEqual({
        tasks: [
          { name: "Test2", description: "Desc2", id: expect.any(String) },
          { name: "Test3", description: "Desc3", id: expect.any(String) },
        ],
      });
    });
  });
});
