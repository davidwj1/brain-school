const TaskRepository = require("./task-repository");

class InMemoryStorage extends TaskRepository {
  constructor() {
    super();
    this.tasks = new Map();
  }

  async create(task) {
    this.tasks.set(task.id, task);
  }
  async getAll() {
    const iterator = this.tasks.values();
    return { tasks: Array.from(iterator) };
  }
  async getById(id) {
    const task = this.tasks.get(id);
    if (task !== undefined) {
      return task;
    }
    return {};
  }

  async searchByName(taskName) {
    const testArr = []
    for (let [key, value] of this.tasks.entries()) {
      if(value.name === taskName){
        testArr.push(value)
      }
    }
    return testArr;
  }

  async update(task) {
    this.tasks.set(task.id, task);
  }
  async delete(id) {
    this.tasks.delete(id);
  }
}

module.exports = InMemoryStorage;
