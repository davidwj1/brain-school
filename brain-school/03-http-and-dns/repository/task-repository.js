class TaskRepository {
    async create() {}
    async getAll() {}
    async getById() {}
    async update() {}
    async delete() {}
    async searchByName() {}
}

module.exports = TaskRepository